<!DOCTYPE html>
<html>
    <head>
        <title>Validacion de formulario</title>
    </head>
    <body>

      <?php 
        
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $edad = $_POST['edad'];
        $direccion = $_POST['direccion'];

        echo "<br /><br />Hola yo soy ".$nombre." ".$apellido.". <br /><br />"; 
        echo "Tengo ".$edad." años y vivo en ".$direccion.". <br /><br />";

        /*----- Verifica si es mayor o menor ----*/

        if($edad >= 18){

          echo "Soy mayor de edad. <br /><br />";

        } else {

          echo "Soy menor de edad. <br /><br />";
        }
        
        /* ----- Con Radio verifica si tiene estudio --*/

        if($_POST['button'] == "sin estudio"){

          echo "No tengo estudio. <br /><br />";

        } elseif($_POST['button'] == "primario"){

          echo "Poseo estudio primario. <br /><br />";
        
        } elseif($_POST['button'] == "secundario"){

          echo "Poseo estudio seundario. <br /><br />";
        }

        /*-----Con select verificar si hombre o mujer --*/

        if($_POST['menu'] == "masculino"){

          echo "Sexo maculino. <br /><br />";
        
        } elseif($_POST['menu'] == "femenino"){

          echo "Sexo femenino. <br /><br />";
        }

        /*---Con Radio verifica los deporte que realiza --*/ 

        $arregloDeporte = [];

        $arregloDeporte[] = $_POST['button1'];
        $arregloDeporte[] = $_POST['button2'];
        $arregloDeporte[] = $_POST['button3'];
        $arregloDeporte[] = $_POST['button4'];

        foreach($arregloDeporte as $deporte){

          if($deporte == "futbol"){
            echo "El deporte que practico es ".$deporte."<br /><br />";
          } 

          if($deporte == "basquet"){
            echo "El deporte que practico es ".$deporte."<br /><br />";
          }

          if($deporte == "tennis"){
            echo "El deporte que practico es ".$deporte."<br /><br />";
          }

          if($deporte == "voley"){  
            echo "El deporte que practico es ".$deporte."<br /><br />";
          }
        }

      ?>

      <a href="formularioConGet.html">Ir a la pagina principal</a>
    </body>
</html>