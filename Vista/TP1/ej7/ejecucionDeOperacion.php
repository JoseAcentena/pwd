<!DOCTYPE html>
<html>

  <head>
    <title>Ejercicio número siete</title>
  </head>
  <body>

  <h3> Resultado de la operación seleccionada </h3>

  <form method="$_POST" action="formularioDeOperacion.html">
   
    <?php
        
        $primerNumero = $_POST['primero'];
        $segundoNumero = $_POST['segundo'];
    
        if($_POST['menu'] == "Suma"){
            
            $total = $primerNumero + $segundoNumero;
            echo $primerNumero." + ".$segundoNumero." = ".$total."<br /><br />";
        
        }elseif($_POST['menu'] == "Resta"){
            
            $total = $primerNumero - $segundoNumero;
            echo $primerNumero." - ".$segundoNumero." = ".$total."<br /><br />";
        
        }elseif($_POST['menu'] == "Multiplicacion"){
            
            $total = $primerNumero * $segundoNumero;
            echo $primerNumero." * ".$segundoNumero." = ".$total."<br /><br />";
        
        }elseif($_POST['menu'] == "Division"){
            
            $total = $primerNumero / $segundoNumero;
            echo $primerNumero." / ".$segundoNumero." = ".$total."<br /><br />";
        
        }else {

            echo "No a seleccionado ninguna operación <br /><br />";
        }
    
    ?>
   
    <input type="submit" value="Volver">
   
  </form>
  </body>
</html>