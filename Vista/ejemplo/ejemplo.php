<!DOCTYPE html>
<html>
  
  <head>
    <title>Ejemplo de Prueba</title>
  </head>
  <body>
    
    <?php
       
       session_start();
       if(!isset($_SESSION['datos'])){

           $_SESSION['datos'] = array();
       }
    ?>

    <form method="get" action="validar.php">
          
        <label for="valor">Valor</label><br/>
        <input type="text" name="valor" id="valor">

        <input type="hidden" name="valores" value="<?php echo implode(',', $_SESSION['datos']) ?>">

        <input type="submit" value="Enviar array">
    </form>
  
  </body>
</html>